import 'package:flutter/material.dart';
import './question.dart';
import './answer.dart';

class Quiz extends StatelessWidget {
  final Function onButtonPressed;
  final List<Map<String, Object>> questionList;
  final int questionIndex;

  Quiz({
    @required this.questionList,
    @required this.questionIndex,
    @required this.onButtonPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Question(questionList[questionIndex]['question']),
        ...(questionList[questionIndex]['answers'] as List<Map<String, Object>>)
            .map((answer) {
          return Answer(() => onButtonPressed(answer['score']), answer['text'],
              answer['buttonColour']);
        }).toList()
      ],
    );
  }
}
