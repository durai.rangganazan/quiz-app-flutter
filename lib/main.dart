import 'package:flutter/material.dart';
import 'package:quiz_app/result.dart';
import './quiz.dart';
import './result.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;
  var _totalScore = 0;

  var _questionList = [
    {
      'question': 'What\'s your favourite colour?',
      'answers': [
        {'text': 'Red', 'score': 1, 'buttonColour': Colors.red},
        {'text': 'Blue', 'score': 3, 'buttonColour': Colors.blue},
        {'text': 'Yellow', 'score': 5, 'buttonColour': Colors.yellow.shade600},
        {'text': 'Green', 'score': 10, 'buttonColour': Colors.green}
      ],
    },
    {
      'question': 'What\'s your favourite animal?',
      'answers': [
        {'text': 'Tiger', 'score': 10, 'buttonColour': Colors.orange},
        {'text': 'Monkey', 'score': 5, 'buttonColour': Colors.brown},
        {'text': 'Cat', 'score': 1, 'buttonColour': Colors.white12},
        {'text': 'Dog', 'score': 3, 'buttonColour': Colors.black}
      ],
    },
    {
      'question': 'What\'s your favourite drink?',
      'answers': [
        {'text': 'Water', 'score': 3, 'buttonColour': Colors.lightBlue},
        {
          'text': 'Bubble Tea',
          'score': 10,
          'buttonColour': Colors.orange.shade200
        },
        {'text': 'Milo', 'score': 5, 'buttonColour': Colors.brown},
        {'text': 'Coffee', 'score': 1, 'buttonColour': Colors.black}
      ],
    },
  ];

  void _onButtonPressed(int score) {
    _totalScore += score;
    setState(() {
      _questionIndex++;
    });
  }

  void resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.teal[400],
            centerTitle: true,
            title: Text(
              "Know Yourself",
            ),
          ),
          body: _questionIndex < _questionList.length
              ? Quiz(
                  onButtonPressed: _onButtonPressed,
                  questionList: _questionList,
                  questionIndex: _questionIndex,
                )
              : Result(_totalScore, resetQuiz)),
    );
  }
}
