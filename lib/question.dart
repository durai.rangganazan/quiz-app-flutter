import 'package:flutter/material.dart';

class Question extends StatelessWidget {
  String question;

  Question(String question) {
    this.question = question;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(top: 100, bottom: 100),
      child: Text(
        question,
        style: TextStyle(fontSize: 22),
        textAlign: TextAlign.center,
      ),
    );
  }
}
