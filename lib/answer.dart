import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  final Function answerHandler;
  final String answer;
  final Color buttonColour;

  Answer(this.answerHandler, this.answer, this.buttonColour);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      height: 80,
      padding: EdgeInsets.all(10),
      child: RaisedButton(
        color: buttonColour,
        textColor: Colors.white,
        child: Text(
          answer,
          style: TextStyle(fontSize: 18),
        ),
        onPressed: answerHandler,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(30.0),
        ),
      ),
    );
  }
}
