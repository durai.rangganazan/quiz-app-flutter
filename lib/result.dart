import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int totalScore;
  final Function resetQuiz;

  Result(this.totalScore, this.resetQuiz);

  String get finalResultText {
    String finalText;

    if (totalScore <= 3) {
      finalText =
          "You have such awesome character!\n You have no haters as everyone loves you!";
    } else if (totalScore <= 9) {
      finalText =
          "You definitely are a great personality!\n You are natural leader!";
    } else if (totalScore <= 15) {
      finalText =
          "You have such a ... strange character!\n You have to work on it!";
    } else {
      finalText = "Are you ok?\n Seems like you have a problem...";
    }
    return finalText;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(top: 100),
      child: Column(children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 5.0, right: 5.0, bottom: 50.0),
          child: Text(
            finalResultText,
            style: TextStyle(fontSize: 42, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),
        RaisedButton(
          padding: EdgeInsets.all(30),
          color: Colors.teal.shade400,
          textColor: Colors.white,
          child: Text(
            "Retake the Quiz?",
            style: TextStyle(fontSize: 18),
          ),
          onPressed: resetQuiz,
          shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(30.0),
          ),
        )
      ]),
    );
  }
}
